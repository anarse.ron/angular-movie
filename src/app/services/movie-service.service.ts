import { Injectable } from '@angular/core';
import { Movies } from '../movies/movies.model';
@Injectable({
  providedIn: 'root'
})
export class MovieServiceService {
  private movie: Movies[] = [
    {
      title: 'Shawshank Redemption',
      imageUrl:
      'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg',
       timings: ['10AM', '12PM', '8PM']
    },
    {
      title: 'Batman Vs Superman',
      imageUrl:
        'https://m.media-amazon.com/images/M/MV5BYThjYzcyYzItNTVjNy00NDk0LTgwMWQtYjMwNmNlNWJhMzMyXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
      timings: ['9AM', '4PM', '7PM']
    },
    {
      title: 'Avengers Endgame',
      imageUrl:
        'https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg',
      timings: ['9AM', '4PM', '6PM']
    },
    {
      title: 'Harry Potter',
      imageUrl:
        'https://m.media-amazon.com/images/M/MV5BNjQ3NWNlNmQtMTE5ZS00MDdmLTlkZjUtZTBlM2UxMGFiMTU3XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_SX300.jpg',
      timings: ['9AM', '2PM', '4PM']
    },
    {
      title: 'Aquaman',
      imageUrl:
        'https://m.media-amazon.com/images/M/MV5BOTk5ODg0OTU5M15BMl5BanBnXkFtZTgwMDQ3MDY3NjM@._V1_SX300.jpg',
      timings: ['9AM', '6PM', '7PM']
    }
  ];

  constructor() { }

  getAllmovieList(){
    return [...this.movie]
  }
}
