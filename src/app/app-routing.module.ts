import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingsComponent } from './movies/bookings/bookings.component';
import { BookmovieComponent } from './movies/bookmovie/bookmovie.component';
import { HomeComponent } from './movies/home/home.component';

const routes: Routes = [
  
    {path: '', redirectTo: 'movies/home', pathMatch: 'full'},
    {path: 'movies/home', component: HomeComponent},
    {path: 'movies/bookmovie', component: BookmovieComponent},
    {path: 'movies/bookings', component: BookingsComponent},
    { path: '**', redirectTo: 'movies/home' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
