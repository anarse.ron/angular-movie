export interface Movies{
    title : string,
    imageUrl: string;
    timings: string[];
}