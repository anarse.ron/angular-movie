import { Component, OnInit } from '@angular/core';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { Movies } from '../movies.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-bookmovie',
  templateUrl: './bookmovie.component.html',
  styleUrls: ['./bookmovie.component.scss']
})
export class BookmovieComponent implements OnInit {

  moviesData : Movies[];
  selectedMovieList :any = [];
  timings = [];
  bookingForm: FormGroup;
  submitted = false;
  bookingFormValue;
  mydata;


  constructor(private movieService : MovieServiceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getMovies();

    this.bookingForm = this.formBuilder.group({
      selectedMovie : ['', Validators.required],
      selectedTime : ['', Validators.required],
      totalTicket : ['', Validators.required]
    })

  }

  get f(){
    return this.bookingForm.controls;
  }
  

  getMovies(){
   this.moviesData = this.movieService.getAllmovieList();
    console.log(this.moviesData)
  }

  onMoviechange(showtTimings){
     this.selectedMovieList = this.moviesData.find(movieTime => movieTime.title == showtTimings).timings
     console.log(this.selectedMovieList)
  }

  
  onSubmit(){
    this.submitted = true;
    if(this.bookingForm.valid){
      this.bookingFormValue = this.bookingForm.value
      console.log(this.bookingForm.value);
       this.mydata = JSON.parse(localStorage.getItem('bookedmovie')) || [];
      this.mydata.push(this.bookingForm.value)
      localStorage.setItem('bookedmovie', JSON.stringify(this.mydata))
      Swal.fire('Your Booking is confirmed Successfully!')
      this.bookingForm.reset();
    }
  }
}
