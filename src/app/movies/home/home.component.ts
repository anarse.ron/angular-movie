import { Component, OnInit } from '@angular/core';
import { MovieServiceService } from 'src/app/services/movie-service.service';
import { Movies } from '../movies.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  moviesData : Movies[];

  constructor(private movieService : MovieServiceService) { }

  ngOnInit(): void {
    this.getMovies();
  }

  getMovies(){
   this.moviesData = this.movieService.getAllmovieList();
    console.log(this.moviesData)
  }
}
